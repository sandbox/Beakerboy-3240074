<?php
namespace Drupal\views_union\Plugin\views\union;
/**
 * Default implementation of the base union plugin.
 *
 * @ingroup views_union_handlers
 *
 * @ViewsUnion("standard")
 */
class Standard extends UnionPluginBase {
}
