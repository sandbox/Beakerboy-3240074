<?php
namespace Drupal\views_union\Plugin\views\union;
use Drupal\views\Plugin\views\BrokenHandlerTrait;
/**
 * A special handler to take the place of missing or broken handlers.
 *
 * @ingroup views_union_handlers
 *
 * @ViewsUnion("broken")
 */
class Broken extends UnionPluginBase {
  use BrokenHandlerTrait;
}
